let effects = [];

function effectsEqual(arr1, arr2) {
  if (!arr1 || !arr2) return false;

  let equal = true;
  for (let i = 0; i < arr1.length; i++) {
    if (Object.is(arr1[i], arr2[i])) {
      equal = false;
      break;
    }
  }

  return equal;
}

export const runEffects = () => {
  effects.forEach((e) => {
    const nextDeps = e.deps();
    if (!effectsEqual(e.cloneDps, nextDeps)) {
      e.cb();
      e.cloneDps = nextDeps;
    }
  });
};

export const useEffect = (cb, deps) => {
  effects.push({
    cb,
    deps
  });
};
